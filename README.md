[![MIT Licensed](https://img.shields.io/apm/l/vim-mode)](https://gitlab.com/jracevedob/thefifthdriver/-/blob/master/LICENSE)

## The fifth driver

The Fifth Driver is a project for the Adaptative Computing Developer Contest for Xilinx. We pretend to demonstrate that the Xilinx Ultrascale+ MPSoC architecture is suitable for developing machine learning applications applied to Autonomous Driving.

## Table of Contents

*   [Overview](#overview)
*   [Catalog](#catalog)
*   [Quick Start](#quick-start)
*   [Citing Our Works](#citing-our-works)
*   [Contributing](#contributing)
*   [Contact](#contact)
*   [License](#license)


## Contributing

This project exists thanks to Xilinx and Hackster for promoting and supporting  the Adaptative Computing Developer Contest with hardware and interactive expert technical support.
We also would like to thank to the reviewers of this work for porting and validating our findings. 

## Contributors and maintainers

Cristian Perez - Paris   - Project maintainer and software developer - felipebrokate at gmail dot com

Javier Acevedo - Dresden - Project maintainer and hardware developer - jracevedob at gmail dot com

## License

This project is licensed under the [MIT license](./LICENSE).
