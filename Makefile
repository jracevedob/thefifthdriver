#
# Makefile
#

BASH_SRCS := $(shell find ./ -name '*.sh')
PY_SRCS := $(shell find ./ -name '*.py')
ALL_SCRIPT_SRCS := $(BASH_SRCS) $(PY_SRCS)

all:
	@echo "Makefile needs your attention"

codecheck-scripts: $(ALL_SCRIPT_SRCS)
	@echo "* Run flake8 for Python sources..."
	flake8 --ignore=E501,E266,E203,E231,W503,F401,F841 $(PY_SRCS)
	@echo "* Run shellcheck for BASH sources..."
	shellcheck $(BASH_SRCS)

format-scripts: $(ALL_SCRIPT_SRCS)
	@echo "* Format all Python sources with black..."
	black $(PY_SRCS)
	@echo "* Format all Bash sources with shfmt..."
	shfmt -i 4 -w $(BASH_SRCS)

## Cleanup utilities

rm-all-containers:
	@echo "Remove all docker containers"
	docker container rm $$(docker ps -aq) -f

rm-dangling-images:
	@echo "Remove all dangling docker images"
	docker rmi $$(docker images -f "dangling=true" -q)

pp-empty-dirs:
	@echo "Print empty directories"
	@find -maxdepth 3 -type d -empty

# Install python requirements

Software/.env:
	cp Software/.env.sample Software/.env

Software/venv:
	python3 -m venv Software/venv;
	Software/venv/bin/pip install -U -r Software/requirements.txt;

Software/models/ssd_mobilenet/frozen_inference_graph.pb:
	curl -o ssd_mobilenet.tar.gz http://download.tensorflow.org/models/object_detection/ssd_mobilenet_v2_coco_2018_03_29.tar.gz
	tar xvzf ssd_mobilenet.tar.gz -C Software/models/ssd_mobilenet --strip-components=1
	rm -rf ssd_mobilenet.tar.gz

build: Software/venv Software/models/ssd_mobilenet/frozen_inference_graph.pb Software/.env

up: build
	FLASK_APP=Software/backend/app.py ./Software/venv/bin/flask run 

# vim:ft=make
#

