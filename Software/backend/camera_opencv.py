import os
import cv2
import glob
import time
from functools import reduce
from dotenv import load_dotenv
from importlib import import_module
from datetime import datetime, timedelta
from .base_camera import BaseCamera
from .utils import reduce_tracking
from time import sleep

load_dotenv()
Detector = import_module('backend.' + os.environ['DETECTION_MODEL']).Detector
detector = None
ct = None

IMAGE_FOLDER = "imgs"


class Camera(BaseCamera):
    # default value
    video_source = 0
    rotation = None

    def set_video_source(self, source):
        self.video_source = source

    def frames(self):
        camera = cv2.VideoCapture(self.video_source)
        if not camera.isOpened():
            raise RuntimeError('Could not start camera.')

        while True:
            # read current frame
            _, img = camera.read()

            if self.rotation:
                if self.rotation == '90':
                    img = cv2.rotate(img, cv2.ROTATE_90_CLOCKWISE)
                if self.rotation == '180':
                    img = cv2.rotate(img, cv2.ROTATE_180)
                if self.rotation == '270':
                    img = cv2.rotate(img, cv2.ROTATE_90_COUNTERCLOCKWISE)
            ## encode as a jpeg image and return it
            #yield cv2.imencode('.jpg', img)[1].tobytes()
            sleep(1)
            yield img


def load_detector():
    global detector, ct
    detector = Detector()

def CaptureContinous():
    global detector
    if detector is None:
        load_detector()
    cap = cv2.VideoCapture(0)
    _, image = cap.read()
    cap.release()
    output = detector.prediction(image)
    df = detector.filter_prediction(output, image)
    if len(df) > 0:
        if (df['class_name']
                .str
                .contains('person|bird|cat|wine glass|cup|sandwich')
                .any()):
            day = datetime.now().strftime("%Y%m%d")
            directory = os.path.join(IMAGE_FOLDER, 'webcam', day)
            if not os.path.exists(directory):
                os.makedirs(directory)
            image = detector.draw_boxes(image, df)
            classes = df['class_name'].unique().tolist()
            hour = datetime.now().strftime("%H%M%S")
            filename_output = os.path.join(
                    directory, "{}_{}_.jpg".format(hour, "-".join(classes))
                    )
            cv2.imwrite(filename_output, image)


class Predictor(object):

    def prediction(self, img, conf_th=0.3, conf_class=[]):
        global detector
        if detector is None:
            load_detector()
        output = detector.prediction(img)
        df = detector.filter_prediction(output, img, conf_th=conf_th, conf_class=conf_class)
        img = detector.draw_boxes(img, df)
        return img

if __name__ == '__main__':
    predictor = Predictor()
    CaptureContinous()
